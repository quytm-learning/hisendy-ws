# Hisendy Webservice - Chat application

Hisendy là một ứng dụng chat real-time.

Hisendy Webservice sử dụng API và socketIO để kết nối, trao đổi dữ liệu, message giữa 
client và server, giữa các người dùng với nhau.

## I. Technologies & Libraries

- [NodeJS](https://nodejs.org)
- [ExpressJS](http://expressjs.com)
- [SocketIO](https://socket.io)
- [MongoDB](https://www.mongodb.com) - [Mongoose](http://mongoosejs.com)

## II. Implementations

Các bước cài đặt server:

- `npm install`
- Tạo file `config.dev.json` với các thông số tham khảo trong file `config.example.json 
- `require` file `seedData.dev.js` để seed một số dữ liệu cho DB
- Start ứng dụng bằng lệnh: `npm start` hoặc `npm nodemon`

## III. Project structure

...

![project_structure](http://prntscr.com/keaqbh)


## IV. Testing

*(to be continue ...)*

