'use strict';

const router = require('express').Router();

const helloController = require('./controllers/hello.controller');
let userController = require('./controllers/user.controller');
let roomController = require('./controllers/room.controller');
let messageController = require('./controllers/message.controller');

module.exports = () => {

    router.all('/', (req, res) => res.send('Hello!'));
    router.all('/ping', (req, res) => res.send('pong'));

    router.use('/hello', helloController());
    // Declaring another controllers
    router.use('/api/users', userController());
    router.use('/api/rooms', roomController());
    router.use('/api/messages', messageController());

    return router;
};