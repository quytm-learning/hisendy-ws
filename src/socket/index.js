'use strict';

const {CONNECTION} = require('./action');

module.exports = (app) => {

    let server = require('http').Server(app);
    let io = require('socket.io')(server);

    let chatEvent = require('./chat.event');
    let roomEvent = require('./room.event');

    // All action associated rooms: create room, ...
    io.of('/rooms').on(CONNECTION, roomEvent);
    // All action associated chats: send message, ...
    io.of('/chats').on(CONNECTION, chatEvent);

    return server;

};
