'use strict';

let logger = require('../utils/logger');
let roomService = require('../services/room.service');

const {
    CREATE_ROOM,
    UPDATE_ROOM
} = require('./action');

module.exports = (socket) => {

    logger.log('room.event', 'Connect to Room space');

    socket.on(CREATE_ROOM, createRoomEvent);

    // -----------------------------------------------------------------------------------------------------------------

    // async function createRoomEvent(data) {
    //     logger.log('createRoomEvent', `data = ${JSON.stringify(data)}`);
    //     let newRoom = await roomService.createNewRoom(data.leftUser, data.rightUser);
    //     if (newRoom != null) {
    //         console.log('New room = ' + newRoom);
    //         socket.emit(Event.UPDATE_ROOM, {message: 'Bắt đầu trò chuyện', room: newRoom});
    //         socket.broadcast.emit(Event.UPDATE_ROOM, {message: 'Có room mới kìa!', room: newRoom});
    //     } else {
    //         console.log('Error when create new Room');
    //     }
    // }

    function createRoomEvent(data) {
        logger.log('createRoomEvent', `data = ${JSON.stringify(data)}`);
        roomService.createNewRoom(data.leftUser, data.rightUser)
            .then((newRoom) => {
                if (newRoom != null) {
                    console.log('New room = ' + newRoom);
                    socket.emit(UPDATE_ROOM, {message: 'Bắt đầu trò chuyện', room: newRoom});
                    socket.broadcast.emit(UPDATE_ROOM, {
                        message: 'Có room mới kìa!',
                        room: newRoom
                    });
                } else {
                    console.log('Error when create new Room');
                }
            });
    }

};