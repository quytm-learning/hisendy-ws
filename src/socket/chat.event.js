'use strict';

let logger = require('../utils/logger');
const {
    DISCONNECT,
    JOIN_ROOM,
    NEW_MESSAGE,
    ADD_MESSAGE
} = require('./action');
let messageService = require('../services/message.service');

module.exports = (socket) => {

    logger.log('chat.event', 'Connect to Chat space');

    socket.on(JOIN_ROOM, joinRoom);

    socket.on(NEW_MESSAGE, newMessage);

    socket.on(DISCONNECT, disconnect);

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * When two users are connected, they must join into same room
     * User send JOIN_ROOM event => server will join this socket(created by user) into room (in socketIO) by roomId
     * @param room: contain roomId
     */
    function joinRoom(room) {
        let roomId = room._id;
        logger.log('joinRoom', `Join into roomId = ${roomId}`);
        socket.join(roomId);
    }

    /**
     * When user send a message, socketIO receive NEW_MESSAGE event
     * => message is saved to DB
     * => message is sent to another user which had joined into room
     * @param message: contain: author, content, room
     */
    function newMessage(message) {
        console.log(JSON.stringify(message));
        let {author, content, room} = message;
        logger.log('chat.event', `New message from ${author} to room ${room} with content = ${JSON.stringify(content)}`);

        messageService.addNewMessage(message)
            .then(newMessage => {
                return messageService.findLastMessageInRoom(message.room);
            })
            .then(lastMessage => {
                // socket.to(roomId).emit(ADD_MESSAGE, data);
                socket.broadcast.to(room).emit(ADD_MESSAGE, lastMessage);
            })
            .catch(err => {
                logger.error('chat.event', `Error: ${err}`);
            })
    }

    /**
     * When user disconnect socket: close browser, disconnect network, ...
     */
    function disconnect() {
        logger.log('chat.event', `Client is disconnect!`);
        // socket.leave(<roomId>): todo: leave room
    }
};