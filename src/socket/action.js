'use strict';

module.exports = {
    // For all
    CONNECTION: 'connection',
    DISCONNECT: 'disconnect',


    // For message
    NEW_MESSAGE: 'newMessage',
    ADD_MESSAGE: 'addMessage',


    // For rooms
    CREATE_ROOM: 'createRoom',
    UPDATE_ROOM: 'updateRoom',
    JOIN_ROOM: 'joinRoom'
};