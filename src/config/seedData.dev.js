let seeder = require('mongoose-seed');
const config = require('./config.dev.json');
let Glob = require('glob');

// Connect to MongoDB via Mongoose
seeder.connect(config.MONGODB_URI, function () {

    // Load Mongoose models
    let models = Glob.sync('src/models/*.js');
    seeder.loadModels(models);

    // seeder.loadModels([
    //     'app/model1File.js',
    //     'app/model2File.js'
    // ]);

    // Clear specified collections
    seeder.clearModels(['User', 'Room', 'Message'], function () {

        // Callback to populate DB once collections have been cleared
        seeder.populateModels(data, function () {
            seeder.disconnect();
        });

    });
});

// Data array containing seed data - documents organized by Model
const data = [
    {
        'model': 'User',
        'documents': [
            {
                name: 'Trần Minh Quý',
                email: 'minhquylt95@gmail.com',
                sex: 'male',
                bio: ''
            },
            {
                name: 'Ngô Thành Lê',
                email: 'lent@higgsup.com',
                sex: 'male',
                bio: ''
            },
            {
                name: 'Trần Thị Huệ',
                email: 'huett@higgsup.com',
                sex: 'female',
                bio: ''
            },
            {
                name: 'Lê Hà Tú',
                email: 'tulh52@gmail.com',
                sex: 'male',
                bio: ''
            },
        ]
    },
    {
        'model': 'Room',
        'documents': [
            {
                leftUser: '123',
                rightUser: '456',
                createAt: Date.parse('2018-07-01')
            }
        ]
    }
];

//{
//  "laptop": {
//    "users": [
//      {
//        "name": "Trần Minh Quý",
//        "id": "5b61b061865b323e384d69bd"
//      },
//      {
//        "name": "Ngô Thành Lê",
//        "id": "5b61b061865b323e384d69be"
//      },
//      {
//        "name": "Trần Thị Huệ",
//        "id": "5b61b061865b323e384d69bf"
//      },
//      {
//        "name": "Lê Hà Tú",
//        "id": "5b61b061865b323e384d69c0"
//      }
//    ]
//  }
//}