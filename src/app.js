'use strict';

const express = require('express');
const logger = require('./utils/logger');
const config = require('./config/config.dev.json');
const database = require('./database');
const app = express();

/**
 *  After connected to MongoDB => start server
 */
new Promise((resolve, reject) => {
    database.connect(config)
        .then((msg) => {

            logger.log('app', `After connect to database, msg = ${msg}`);

            const bodyParser = require('body-parser');
            const appRouter = require('./app.routes');
            const cors = require('cors');

            const port = process.env.PORT || config.PORT || 3000;

            // Support some middleware
            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded({extended: false}));
            app.use(cors());

            // For router
            app.use(appRouter());

            // For socket
            let server = require('./socket')(app);

            server.listen(port, (error) => {
                if (error) throw error;
                logger.log('app', `Listening on port ${port}`);
            });

            // For seeding data
            // require('./config/seedData.dev');

            resolve();
        })
        .catch(reject);
});
