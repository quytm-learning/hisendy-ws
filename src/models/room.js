'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let RoomSchema = new Schema({

    leftUser: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    rightUser: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now()
    }

});

let Room = mongoose.model('Room', RoomSchema);

module.exports = Room;