'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        index: true,
        trim: true
    },
    avatar: {
        type: String,
        default: 'https://www.gmatamsterdam.com/wp-content/uploads/2015/07/vraagteken-in-cirkels_318-57711.jpg'
    },
    sex: {
        type: String,
        enum: ['male', 'female']
    },
    bio: {
        type: String
    }
});

let User = mongoose.model('User', UserSchema);

module.exports = User;