'use strict';

const api = require('express').Router();
const logger = require('../utils/logger');
const messageService = require('../services/message.service');
const Message = require('../models/message');

/**
 * Message Controller
 * Base url = /api/messages/...
 * @returns {Router|router}
 */
module.exports = () => {

    /**
     * Add new message
     * Payload = {
     *     author   : <user id>,
     *     room     : <room id>,
     *     content  : <content of message>
     * }
     * @return void
     */
    api.post('/', addNewMessage);

    /**
     * Find last message
     * @param @roomId: user id
     * @return Message
     */
    api.get('/:roomId/messages/last', findLastMessageInRoom);

    // -----------------------------------------------------------------------------------------------------------------

    function addNewMessage(req, res) {

        let msg = req.body;

        messageService.addNewMessage(msg)
            .then(result => {
                logger.log('message.controller', `Message is added successfully: ${result}`);
                res.json(result);
            })
            .catch(err => {
                logger.log('message.controller', `Error when add message: error = ${err}`);
                res.json({error: err});
            })

    }

    function findLastMessageInRoom(req, res) {

        let roomId = req.params.roomId;

        messageService.findLastMessageInRoom(roomId)
            .then(message => {
                logger.log('message.controller', `Last message = ${message}`);
                res.json(message);
            })
            .catch(err => {
                logger.log('message.controller', `Cannot get last message: error = ${err}`);
                res.json({error: err});
            })
    }

    return api;
};