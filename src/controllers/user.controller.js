'use strict';

const api = require('express').Router();
const logger = require('../utils/logger');
const userService = require('../services/user.service');
const roomService = require('../services/room.service');
const messageService = require('../services/message.service');
const Room = require('../models/room');
const Message = require('../models/message');

/**
 * User Controller
 * Base url = /api/users/...
 * @returns {Router|router}
 */
module.exports = () => {

    /**
     * Find all users which chat with @userId in the past
     * @userId: user id
     * @return [User]
     */
    api.get('/:userId/friends', findAllFriendsByUserId);

    /**
     * Find all rooms which user has joined
     * @userId: user id
     * @return [Room]
     */
    api.get('/:userId/rooms', findAllRoomByUserId);

    /**
     * Get user information by @userId
     * @userId: user id
     * @return User
     */
    api.get('/:userId', getUserInfoById);

    /**
     * Get all messages are sent by @userId
     * @userId: user id
     * @return [Message]
     */
    api.get('/:userId/messages', getAllMessageByUser);

    // -----------------------------------------------------------------------------------------------------------------
    function findAllFriendsByUserId(req, res) {

        let userId  = req.params.userId;

        userService.findAllFriendsByUserId(userId)
            .then(friends => {
                logger.log('user.controller', `Friend list size = ${friends.length}`);
                res.json(friends);
            })
            .catch(error => {
                logger.error('user.controller', `Error in /friends: ${error}`);
                res.json({message: error});
            });
    }

    function findAllRoomByUserId(req, res) {

        let userId = req.params.userId;
        roomService.findAllRoomByUserId(userId)
            .then(rooms => {
                res.json(rooms);
            })
            .catch(err => {
                res.json({message: err});
            })

    }

    function getUserInfoById(req, res) {

        let userId = req.params.userId;
        userService.getUserInfoById(userId)
            .then(user => {
                res.json(user);
            })
            .catch(err => {
                res.json({error: err});
            })

    }

    function getAllMessageByUser(req, res) {

        let userId = req.params.userId;
        return messageService.getAllMessageByUser(userId)
            .then(messages => {
                res.json(messages);
            })
            .catch(err => {
                res.json({error: err});
            })
    }

    return api;
};