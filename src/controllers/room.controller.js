'use strict';

const api = require('express').Router();
const logger = require('../utils/logger');
const roomService = require('../services/room.service');
const Room = require('../models/room');

/**
 * Room Controller
 * Base url = /api/rooms/...
 * @returns {Router|router}
 */
module.exports = () => {

    /**
     * Find all rooms
     * @return [Room]
     */
    api.get('/', findAll);

    /**
     * Create new room
     * Payload = {
     *     leftUser: <first user id>,
     *     rightUser: <second user id>
     * }
     * @return Room
     */
    api.post('/', createNewRoom);

    // -----------------------------------------------------------------------------------------------------------------

    function findAll(req, res) {
        return roomService.findAll()
            .then(rooms => {
                res.json(rooms);
            })
            .catch(err => {
                res.json({message: err});
            })
    }

    function createNewRoom(req, res) {

        let leftUId = req.body.leftUser;
        let rightUId = req.body.rightUser;

        roomService.createNewRoom(leftUId, rightUId)
            .then(room => {
                logger.log('room.controller', `create new room: ${room}`);
                res.json(room);
            })
            .catch(err => {
                logger.error('room.controller', `err = ${err}`);
                res.json({error: err});
            });

    }

    return api;
};