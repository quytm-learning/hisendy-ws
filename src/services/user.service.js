'use strict';

let _ = require('lodash');
let mongoose = require('mongoose');
let User = mongoose.model('User');
let logger = require('../utils/logger');

let roomService = require('./room.service');

/**
 * Find all users which used to chat with userId
 * @param userId
 * @return {Promise<[User]>}
 * // todo: need to update query, not use roomService to find room, need to use Room to find room then populate user
 */
let findAllFriendsByUserId = (userId) => {

    return roomService.findAllRoomByUserId(userId)
        .then(rooms => {
            if (_.isEmpty(rooms)) return Promise.reject('Invalid input.');

            let promises = [];
            rooms.forEach(room => {
                let friendId = room.leftUser === userId ? room.rightUser : room.leftUser;
                promises.push(
                    User.findOne({_id: friendId})
                        .then((user) => {
                            console.log(JSON.stringify(user));
                            user._doc = Object.assign({room: room}, user._doc);
                            return Promise.resolve(user);
                        })
                );
            });

            return Promise.all(promises);
        })
        .then(users => {
            logger.log('user.service', `Friend list: ${JSON.stringify(users)}`);
            return Promise.resolve(users);
        })
        .catch(err => {
            logger.error('user.service', `Cannot find friend by userId = ${userId}, error = ${err}`);
        })

};

/**
 * Get user information by userId
 * @param userId
 * @return {Promise<User>}
 */
let getUserInfoById = (userId) => {

    return User.findOne({_id: userId});

};

module.exports = {
    findAllFriendsByUserId: findAllFriendsByUserId,
    getUserInfoById: getUserInfoById
};