'use strict';

let _ = require('lodash');
let mongoose = require('mongoose');
let Message = mongoose.model('Message');
let logger = require('../utils/logger');

/**
 * Add new message to DB
 * @param message = {
 *      author   : <user id>,
 *      room     : <room id>,
 *      content  : <content of message>
 * }
 * @return {Promise<Message>}
 */
let addNewMessage = (message) => {

    logger.log('message.service', `New message= ${JSON.stringify(message)}`);

    let newMessage = new Message(message);

    return newMessage.save();
};

/**
 * Get last message in roomId
 * @param roomId: room id
 * @return {Promise<Message>}
 */
let findLastMessageInRoom = (roomId) => {

    return Message.find({room: roomId})
        .populate('room')       // Get room info
        .populate('author')     // Get author info
        .sort({createdAt: -1})  // Last message
        .limit(1)               // Get one record
        .exec()
        .then(messages => {
            if (!_.isEmpty(messages)) {
                return Promise.resolve(messages[0]);
            } else {
                return Promise.resolve(new Object(''));
            }
        });
};

/**
 * Get all messages which is sent by userId
 * @param userId: user id
 * @return {Promise<Message>}
 */
let getAllMessageByUser = (userId) => {

    return Message
        .find({})               // Find all message
        .populate({             // Get room info
            path: 'room',
            match:              // ... with conditional = { only get room info when user belongs to room }
                {
                    $or: [
                        {leftUser: {$eq: userId}},
                        {rightUser: {$eq: userId}}
                    ]
                }
        })
        .populate('author')     // Get author info
        .exec()
        .then(messages => {     // Exclude all message which not have room info
            let newMessages = messages.filter(message => message.room != null);
            return Promise.resolve(newMessages)
        });

};

module.exports = {
    addNewMessage: addNewMessage,
    findLastMessageInRoom: findLastMessageInRoom,
    getAllMessageByUser: getAllMessageByUser
};