'use strict';

let mongoose = require('mongoose');
let Hello = mongoose.model('Hello');

let hello = (name) => {
    return new Promise(((resolve, reject) => {
        resolve({data: `Hello ${name}`});
    }))
};

let findAll = () => {
    return Hello.find();
};

let findByName = (name) => {
    return Hello.find({name: name});
};

module.exports = {
    hello: hello,
    findAll: findAll,
    findByName: findByName
};