'use strict';

let _ = require('lodash');
let mongoose = require('mongoose');
let Room = mongoose.model('Room');
let User = mongoose.model('User');
let logger = require('../utils/logger');

let messageService = require('./message.service');

/**
 * Create a new room
 * @param left: the person who start the conversation
 * @param right: another person
 * @returns {Promise<Room>}
 *          - return error message when input invalid or error happened
 *          - return Promise<Room>:
 *              + If the conversation between {left} and {right} is exist
 *                  -> return room from DB
 *               + Else (mean that the conversation is not existed)
 *                  -> add new room into DB
 *                  -> return new room
 */
let createNewRoom = (left, right) => {
    if (_.isEmpty(left)) return Promise.reject('Invalid input.');
    if (_.isEmpty(right)) return Promise.reject('Invalid input.');
    if (_.isEqual(left, right)) return Promise.reject('Invalid input.');

    return Room
        .findOne({          // Find rooms
            $or: [
                {leftUser: left, rightUser: right},
                {leftUser: right, rightUser: left}
            ]
        })
        .then(room => {     // Find users by input
            if (room) {
                return Promise.reject(`Room is existed for ${left} and ${right}`);
            } else {
                return Promise.all([
                    User.findOne({_id: left}),
                    User.findOne({_id: right})
                ])
            }
        })
        .then(users => {    // Check users are existed?
            if (!users || users.length < 2) {
                return Promise.reject(`Invalid user id`);
            } else {
                let leftUser = users[0];
                let rightUser = users[1];

                let newRoom = new Room({
                    leftUser: leftUser._id,
                    rightUser: rightUser._id
                });

                return newRoom.save();
            }
        })
        .then(room => {
            logger.log('room.service', `Inserted new room to DB: ${room}`);
            return Promise.resolve(room);
        })
        .catch(err => {
            err = `Cannot create new room for ${left} and ${right}: ${err}`;
            logger.error('room.service', err);
            return Promise.reject(err);
        });

};

/**
 * Find all rooms which user has joined
 * @param userId: user id
 * @return {Promise<[Room]>}
 */
let findAllRoomByUserId = (userId) => {
    if (_.isEmpty(userId)) return Promise.reject('Invalid input.');

    return Room
        .find({     // Find rooms
            $or: [
                {leftUser: userId},
                {rightUser: userId}
            ]
        })
        .populate('leftUser')   // Get leftUser info
        .populate('rightUser')  // Get rightUser info
        .exec()
        .then(rooms => {        // Add more info into each room: room_name, room_avatar
            let newRooms = rooms.map(room => {
                let roomName = room.leftUser._id.toString() === userId ? room.rightUser.name : room.leftUser.name;
                let avatar = room.leftUser._id.toString() === userId ? room.rightUser.avatar : room.leftUser.avatar;
                room._doc = Object.assign({
                    name: roomName,
                    avatar: avatar
                }, room._doc);
                return room;
            });
            return Promise.resolve(newRooms);
        })
        .then(rooms => {        // Add last_message into each room

            let promises = [];
            rooms.forEach(room => {
                let roomId = room._id;

                let promise = messageService.findLastMessageInRoom(roomId)
                    .then(lastMessage => {
                        room._doc = Object.assign({lastMessage: lastMessage}, room._doc);
                        return Promise.resolve(room);
                    });
                promises.push(promise);
            });
            return Promise.all(promises);
        })
};

/**
 * Find all rooms in system
 * @return {Promise<[Room]>}
 */
let findAll = () => {
    return Room.find().exec();
};

// ---------------------------------------------------------------------------------------------------------------------

module.exports = {
    findAll: findAll,
    findAllRoomByUserId: findAllRoomByUserId,
    createNewRoom: createNewRoom
};